// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Intelligent_Guards : ModuleRules
{
	public Intelligent_Guards(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
