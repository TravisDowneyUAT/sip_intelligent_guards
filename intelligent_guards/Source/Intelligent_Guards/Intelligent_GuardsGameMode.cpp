// Copyright Epic Games, Inc. All Rights Reserved.

#include "Intelligent_GuardsGameMode.h"
#include "Intelligent_GuardsCharacter.h"
#include "UObject/ConstructorHelpers.h"

AIntelligent_GuardsGameMode::AIntelligent_GuardsGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
