// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Intelligent_GuardsGameMode.generated.h"

UCLASS(minimalapi)
class AIntelligent_GuardsGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AIntelligent_GuardsGameMode();
};



