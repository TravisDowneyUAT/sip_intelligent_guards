// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef INTELLIGENT_GUARDS_Intelligent_GuardsCharacter_generated_h
#error "Intelligent_GuardsCharacter.generated.h already included, missing '#pragma once' in Intelligent_GuardsCharacter.h"
#endif
#define INTELLIGENT_GUARDS_Intelligent_GuardsCharacter_generated_h

#define Intelligent_Guards_Source_Intelligent_Guards_Intelligent_GuardsCharacter_h_12_SPARSE_DATA
#define Intelligent_Guards_Source_Intelligent_Guards_Intelligent_GuardsCharacter_h_12_RPC_WRAPPERS
#define Intelligent_Guards_Source_Intelligent_Guards_Intelligent_GuardsCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Intelligent_Guards_Source_Intelligent_Guards_Intelligent_GuardsCharacter_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAIntelligent_GuardsCharacter(); \
	friend struct Z_Construct_UClass_AIntelligent_GuardsCharacter_Statics; \
public: \
	DECLARE_CLASS(AIntelligent_GuardsCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Intelligent_Guards"), NO_API) \
	DECLARE_SERIALIZER(AIntelligent_GuardsCharacter)


#define Intelligent_Guards_Source_Intelligent_Guards_Intelligent_GuardsCharacter_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAIntelligent_GuardsCharacter(); \
	friend struct Z_Construct_UClass_AIntelligent_GuardsCharacter_Statics; \
public: \
	DECLARE_CLASS(AIntelligent_GuardsCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Intelligent_Guards"), NO_API) \
	DECLARE_SERIALIZER(AIntelligent_GuardsCharacter)


#define Intelligent_Guards_Source_Intelligent_Guards_Intelligent_GuardsCharacter_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AIntelligent_GuardsCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AIntelligent_GuardsCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AIntelligent_GuardsCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AIntelligent_GuardsCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AIntelligent_GuardsCharacter(AIntelligent_GuardsCharacter&&); \
	NO_API AIntelligent_GuardsCharacter(const AIntelligent_GuardsCharacter&); \
public:


#define Intelligent_Guards_Source_Intelligent_Guards_Intelligent_GuardsCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AIntelligent_GuardsCharacter(AIntelligent_GuardsCharacter&&); \
	NO_API AIntelligent_GuardsCharacter(const AIntelligent_GuardsCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AIntelligent_GuardsCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AIntelligent_GuardsCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AIntelligent_GuardsCharacter)


#define Intelligent_Guards_Source_Intelligent_Guards_Intelligent_GuardsCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(AIntelligent_GuardsCharacter, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__FollowCamera() { return STRUCT_OFFSET(AIntelligent_GuardsCharacter, FollowCamera); }


#define Intelligent_Guards_Source_Intelligent_Guards_Intelligent_GuardsCharacter_h_9_PROLOG
#define Intelligent_Guards_Source_Intelligent_Guards_Intelligent_GuardsCharacter_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Intelligent_Guards_Source_Intelligent_Guards_Intelligent_GuardsCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	Intelligent_Guards_Source_Intelligent_Guards_Intelligent_GuardsCharacter_h_12_SPARSE_DATA \
	Intelligent_Guards_Source_Intelligent_Guards_Intelligent_GuardsCharacter_h_12_RPC_WRAPPERS \
	Intelligent_Guards_Source_Intelligent_Guards_Intelligent_GuardsCharacter_h_12_INCLASS \
	Intelligent_Guards_Source_Intelligent_Guards_Intelligent_GuardsCharacter_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Intelligent_Guards_Source_Intelligent_Guards_Intelligent_GuardsCharacter_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Intelligent_Guards_Source_Intelligent_Guards_Intelligent_GuardsCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	Intelligent_Guards_Source_Intelligent_Guards_Intelligent_GuardsCharacter_h_12_SPARSE_DATA \
	Intelligent_Guards_Source_Intelligent_Guards_Intelligent_GuardsCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Intelligent_Guards_Source_Intelligent_Guards_Intelligent_GuardsCharacter_h_12_INCLASS_NO_PURE_DECLS \
	Intelligent_Guards_Source_Intelligent_Guards_Intelligent_GuardsCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> INTELLIGENT_GUARDS_API UClass* StaticClass<class AIntelligent_GuardsCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Intelligent_Guards_Source_Intelligent_Guards_Intelligent_GuardsCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
