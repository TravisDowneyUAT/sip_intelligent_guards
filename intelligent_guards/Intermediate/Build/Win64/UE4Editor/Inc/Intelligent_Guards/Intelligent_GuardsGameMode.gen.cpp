// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Intelligent_Guards/Intelligent_GuardsGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeIntelligent_GuardsGameMode() {}
// Cross Module References
	INTELLIGENT_GUARDS_API UClass* Z_Construct_UClass_AIntelligent_GuardsGameMode_NoRegister();
	INTELLIGENT_GUARDS_API UClass* Z_Construct_UClass_AIntelligent_GuardsGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_Intelligent_Guards();
// End Cross Module References
	void AIntelligent_GuardsGameMode::StaticRegisterNativesAIntelligent_GuardsGameMode()
	{
	}
	UClass* Z_Construct_UClass_AIntelligent_GuardsGameMode_NoRegister()
	{
		return AIntelligent_GuardsGameMode::StaticClass();
	}
	struct Z_Construct_UClass_AIntelligent_GuardsGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AIntelligent_GuardsGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_Intelligent_Guards,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AIntelligent_GuardsGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "Intelligent_GuardsGameMode.h" },
		{ "ModuleRelativePath", "Intelligent_GuardsGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AIntelligent_GuardsGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AIntelligent_GuardsGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AIntelligent_GuardsGameMode_Statics::ClassParams = {
		&AIntelligent_GuardsGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_AIntelligent_GuardsGameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AIntelligent_GuardsGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AIntelligent_GuardsGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AIntelligent_GuardsGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AIntelligent_GuardsGameMode, 1803626543);
	template<> INTELLIGENT_GUARDS_API UClass* StaticClass<AIntelligent_GuardsGameMode>()
	{
		return AIntelligent_GuardsGameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AIntelligent_GuardsGameMode(Z_Construct_UClass_AIntelligent_GuardsGameMode, &AIntelligent_GuardsGameMode::StaticClass, TEXT("/Script/Intelligent_Guards"), TEXT("AIntelligent_GuardsGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AIntelligent_GuardsGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
